package com.springDeep.spring;
// definition:[ˌdefɪˈnɪʃn]
/*
  容器启动后对每个bean进行解析生成beanDefiniton对象
*/
public class BeanDefinition {
    private Class clazz;           // bean的 class
    private String scope;           // singleton / prototype


    public Class getClazz() {
        return clazz;
    }

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }
}
