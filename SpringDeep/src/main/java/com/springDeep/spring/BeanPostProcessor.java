package com.springDeep.spring;

import com.sun.istack.internal.Nullable;

public interface BeanPostProcessor {
    // 如果可以传入NULL值，则标记为@Nullable，如果不可以，则标注为@Nonnull,用于检查
    @Nullable
    Object postProcessBeforeInitialization(Object bean, String beanName);

    @Nullable
    Object postProcessAfterInitialization(Object bean, String beanName);
}
