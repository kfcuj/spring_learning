package com.springDeep.spring;

/*spring框架中提供了该接口*/
public interface InitializingBean {
    void afterPropertiesSet() throws Exception;

}
