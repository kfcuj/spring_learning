package com.springDeep.spring;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
/*Component作用：
       这个注解表明被注解的类是一个组件，当使用基于注解的配置和类路径扫描时，
       使用这些注解的类会被自动检测到。
Indicates that an annotated class is a "component".
Such classes are considered as candidates for auto-detection
when using annotation-based configuration and classpath scanning.
 */
@Retention(RetentionPolicy.RUNTIME)  // jvm中也存在
@Target(ElementType.TYPE)            // 只能放在类上面
public @interface Component {
    String value() default "";

}

// Spring中也有@Component注解如下，
/*

    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    @Indexed
    public @interface Component {
    String value() default "";

    }
 */
