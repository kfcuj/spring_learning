package com.springDeep.spring;
import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.security.spec.RSAOtherPrimeInfo;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/*实现Spring的基于注解的核心容器类*/
public class MyApplicationContext {
    private Class configClass;
    // 单例池
    private ConcurrentHashMap<String,Object> singletonObjects = new ConcurrentHashMap<String, Object>();
    // 解析bean生成的beanDefinition对象
    private ConcurrentHashMap<String,BeanDefinition> beanDefinitionMap = new ConcurrentHashMap<>();
    //
    private List<BeanPostProcessor> beanPostProcessorsList = new ArrayList<>();




    /*容器初始化过程：解析配置类:ComponentScan注解=>扫描路径=>扫描
    *
    * */
    public MyApplicationContext(Class configClass){
        // step1:根据配置类扫描并解析路径下的bean对象，存入beanDefintionMap
        scan(configClass);

        for(Map.Entry<String,BeanDefinition> entry:beanDefinitionMap.entrySet()){
            String beanName = entry.getKey();
            BeanDefinition beanDefinition = entry.getValue();
            if(beanDefinition.getScope().equals("singleton")){
                Object bean = createBean(beanName,beanDefinition);
                singletonObjects.put(entry.getKey(),bean);     // 初始化单例放入单例池
            }


        }
    }
    /*
      根据beanDefinition常见bean的实例

     */
    private Object createBean(String beanName,BeanDefinition beanDefinition){
        Class clazz = beanDefinition.getClazz();
        try{
            /*
               对象实例化过程: 无参构造函数初始化 + 依赖注入
            */
            Object instance = clazz.getDeclaredConstructor().newInstance();  // 无参构造函数初始化

            /*使用BeanPostProcessor的实现类在其他bean依赖注入前处理其他bean*/
            for(BeanPostProcessor beanPostProcessor:beanPostProcessorsList){
                instance = beanPostProcessor.postProcessBeforeInitialization(instance,beanName);
            }

            // 依赖注入(检查class的Field)
            for(Field declaredField: clazz.getDeclaredFields()){
                if(declaredField.isAnnotationPresent(Autowired.class)){
                    /*这里简化了bean注入的方式，实际autowired是根据类型匹配的，此外还有循环依赖问题没有考虑*/
                    Object bean = getBean(declaredField.getName());
                    declaredField.setAccessible(true); // 访问 Field、 Method 和 Constructor 的私有对象，均需 setAccessible。
                    declaredField.set(instance,bean);
                }

                /*检查bean是否实现beanNameAware接口，如果实现则回调接口方法，传入beanName*/
                if(instance instanceof BeanNameAware){
                    ((BeanNameAware) instance).setBeanName(beanName);
                }

                /*检查bean是否实现InitializingBean接口，如果实现则回调接口方法*/
                if(instance instanceof InitializingBean){
                    ((InitializingBean) instance).afterPropertiesSet();
                }
            }
            /*使用BeanPostProcessor的实现类在其他bean依赖注入后处理其他bean*/
            for(BeanPostProcessor beanPostProcessor:beanPostProcessorsList){
                instance = beanPostProcessor.postProcessAfterInitialization(instance,beanName);
            }

            return instance;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }



    /*
       根据配置类扫描路径下的bean对象
       基本流程：
       1) componentScan获取扫描路径
       2) 加载路径下类文件并解析为beanDefinition对象放入map
       @ComponentScan -> 扫描路径 -> BeanDefinition -> BeanDefinitionMap
     */

    private void scan(Class configClass) {
        this.configClass = configClass;
        // step1:获取配置类的注解
        if(!configClass.isAnnotationPresent(ComponentScan.class)){
            System.out.println("配置类没有componentScan注解");
            return;
        }

        ComponentScan componentScanAnnotation = (ComponentScan) configClass.getAnnotation(ComponentScan.class);  // 获取配置类用于扫描的注
        String path = componentScanAnnotation.value();   // achieve value of @Component 即 扫描路径(包名)
        /*
          三种类加载器(负责加载的类的路径不同)：
          1) Bootstrap   -> jre/lib
          2) Extension ->jre/ext/lib
          3) App -> classpath      (Java应用所对应的classpath)
          classpath存放了当前应用的类的字节码文件的目录,比如开发过程中的IDEA的target/classes目录
          每个应用可以通过java提供的类加载类获取目录下面的资源
        */
        ClassLoader classLoader = MyApplicationContext.class.getClassLoader();         // app classLoader
        URL resource = classLoader.getResource(path.replace(".","//"));
        File file = new File(resource.getFile());                                  // 判断是目录还是文件

        if(file.isDirectory()){                                                    // windows下路径中含有中文无法识别
            File[] files = file.listFiles();
            for(File f:files){
                String fileName = f.getAbsolutePath();
                String className = fileName.substring(fileName.indexOf("com"),fileName.indexOf(".class"));
                className = className.replace("\\",".");

                try{
                    Class<?> clazz = classLoader.loadClass(className);    // 加载扫描路径下的类文件
                    if(clazz.isAnnotationPresent(Component.class)){       // 判断这个类是否有@Component注解即是否是放入容器的bean
                        /*扫描阶段对BeanPostProcessor的实例会进行特判，Spring框架中并非如同下面一样进行实例化
                        * 这里不的处理并不正确?
                        * */
                        if(BeanPostProcessor.class.isAssignableFrom(clazz)){
                            BeanPostProcessor instance = (BeanPostProcessor) clazz.getDeclaredConstructor().newInstance();
                            beanPostProcessorsList.add(instance);
                        }


                         Component componentAnnotation = clazz.getDeclaredAnnotation(Component.class);
                         String beanName = componentAnnotation.value();
                         BeanDefinition beanDefinition = new BeanDefinition();
                         beanDefinition.setClazz(clazz);
                        // 1)考虑bean是单例还是多例
                         if(clazz.isAnnotationPresent(Scope.class)){
                             Scope scopeAnnotation  = clazz.getDeclaredAnnotation(Scope.class);
                             beanDefinition.setScope(scopeAnnotation.value());
                         }else{
                             beanDefinition.setScope("singleton");
                         }
                         beanDefinitionMap.put(beanName,beanDefinition);      // 重复该如何解决？
                    }
                }catch (ClassNotFoundException | NoSuchMethodException e){
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Object getBean(String beanName){
        if(beanDefinitionMap.containsKey(beanName)){
            BeanDefinition beanDefinition = beanDefinitionMap.get(beanName);
            if(beanDefinition.getScope().equals("singleton")){ // 单例
                return singletonObjects.get(beanName);         // 从单例池中获取对象
            }else{                                             // 多例
                // 创建bean对象
                return createBean(beanName,beanDefinition);

            }

        }else{
            throw new NullPointerException();
        }
    }

}
