package com.springDeep.spring;

/*实际spring框架中提供了这个接口，用户提供的bean可以实现这个接口获取到bean的名称*/
public interface BeanNameAware {
    void setBeanName(String name);

}
