package com.springDeep.spring;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
/*
   注解作用: 确定容器的配置类的扫描范围
 */
@Retention(RetentionPolicy.RUNTIME)   // 运行时注解，注解不仅被保存到class文件中，jvm加载class文件之后，仍然存在；
@Target(ElementType.TYPE)             // 注解的作用对象限制为类
public @interface ComponentScan {
    String value();                    // string类型存储类扫描的路径
}

/*
// spring中comonentScan注解
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Repeatable(ComponentScans.class)
public @interface ComponentScan {
@AliasFor("basePackages")
String[] value() default {};
@AliasFor("value")
	String[] basePackages() default {};
        Class<?>[] basePackageClasses() default {};
        Class<? extends BeanNameGenerator> nameGenerator() default BeanNameGenerator.class;
	    Class<? extends ScopeMetadataResolver> scopeResolver() default AnnotationScopeMetadataResolver.class;
        ScopedProxyMode scopedProxy() default ScopedProxyMode.DEFAULT;
        String resourcePattern() default ClassPathScanningCandidateComponentProvider.DEFAULT_RESOURCE_PATTERN;
        boolean useDefaultFilters() default true;
        Filter[] includeFilters() default {};
        Filter[] excludeFilters() default {};
        boolean lazyInit() default false;
    @Retention(RetentionPolicy.RUNTIME)
    @Target({})
    @interface Filter {
    FilterType type() default FilterType.ANNOTATION;
    @AliasFor("classes")
    Class<?>[] value() default {};
    @AliasFor("value")
    Class<?>[] classes() default {};
    String[] pattern() default {};
    }
}
 */
