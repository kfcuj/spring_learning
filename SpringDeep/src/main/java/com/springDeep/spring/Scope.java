package com.springDeep.spring;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

// 运行时注解，注解不仅被保存到class文件中，jvm加载class文件之后，仍然存在；
// 注解的作用对象限制为类
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Scope {
    String value();
}
