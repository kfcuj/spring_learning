package com.springDeep.Test;

import com.springDeep.service.OrderService;
import com.springDeep.service.UserService;
import com.springDeep.spring.MyApplicationContext;
// P6
public class Test {
    public static void main(String[] args) {
        MyApplicationContext ac = new MyApplicationContext(AppConfig.class);          // 将配置类的class对象作为参数传入
        /*
             1) 通过注解配置的bean如何扫描并放入容器
             测试orderService中bean的scope的singleton/prototype对对象创建的影响
         */
//        OrderService orderService = (OrderService) ac.getBean("orderService");  // 从Ioc容器中获取对象
//        System.out.println(orderService);
//        orderService = (OrderService) ac.getBean("orderService");  // 从Ioc容器中获取对象
//        System.out.println(orderService);
//        orderService = (OrderService) ac.getBean("orderService");  // 从Ioc容器中获取对象
//        System.out.println(orderService);


        /*
            2) spring中的属性注入(依赖注入)是如何实现的?
            3) spring中bean的接口扩展机制
               --BeanNameAware接口: aware回调，容器调用传入bean名称
               --InitializingBean接口：bean的初始化
               --BeanPostProcessor接口: bean的后置处理

            为什么容器需要对bean提供扩展机制？
             某些情况下，我们开发者希望在
             1) bean实例化之前
             2) bean实例化之后，属性注入之前
             3) bean属性注入之后
             上面的三个时刻，进行一些操作。
             其中BeanPostProcessor接口可以实现在2),3)时刻进行一些操作。
             测试beanPostProcessor的逻辑
        */
        UserService userService = (UserService) ac.getBean("userService");
        userService.test();
        userService.getOrderService().test();   // IDEA调试流程搞清楚为什么打印两个test

        /*P10 AOP的实现*/
    }
}
