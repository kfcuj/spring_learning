package com.springDeep.Test;
import com.springDeep.spring.ComponentScan;

// 注解包的扫描路径
@ComponentScan(value = "com.springDeep.service")
public class AppConfig {

}
