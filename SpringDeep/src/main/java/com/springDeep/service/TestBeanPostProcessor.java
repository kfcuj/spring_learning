package com.springDeep.service;

import com.springDeep.spring.BeanPostProcessor;
import com.springDeep.spring.Component;

/*
    容器在初始化每一个bean的时候都会检查beanPostProcessor接口的实现类，
    如果实现会调调用接口方法进行操作
 */
@Component
public class TestBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) {

        if(bean instanceof OrderService){
            System.out.println("bean依赖注入前");
            OrderService orderService = (OrderService) bean;
            orderService.setTestBeanPostProcessor("在postProcessBeforeInitialization设置bean");
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) {
        if(bean instanceof OrderService){
            System.out.println("bean依赖注入后");
        }
        return bean;
    }
}
