package com.springDeep.service;

import com.springDeep.spring.Autowired;
import com.springDeep.spring.BeanNameAware;
import com.springDeep.spring.Component;
import com.springDeep.spring.InitializingBean;

@Component(value = "userService")
public class UserService implements BeanNameAware, InitializingBean {
    @Autowired
    private OrderService orderService;

    private String beanName;

    public OrderService getOrderService() {
        return orderService;
    }

    public String getBeanName() {
        return beanName;
    }

    public void test(){
        System.out.println();
        System.out.println(orderService);
        System.out.println(beanName);
    }

    /*Spring容器在启动过程中会检查bean是否实现该接口，然后调用接口方法传入beanName*/
    @Override
    public void setBeanName(String name) {
        beanName = name;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("初始化");
    }
}
