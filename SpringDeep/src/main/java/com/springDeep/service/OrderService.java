package com.springDeep.service;

import com.springDeep.spring.Autowired;
import com.springDeep.spring.Component;
import com.springDeep.spring.Scope;

@Component(value = "orderService")
@Scope(value = "prototype")
public class OrderService {      // 使用属性注入的方式注入依赖
    private String testBeanPostProcessor;

    public String getTestBeanPostProcessor() {
        return testBeanPostProcessor;
    }

    public void setTestBeanPostProcessor(String testBeanPostProcessor) {
        this.testBeanPostProcessor = testBeanPostProcessor;
    }

    public void test(){
        System.out.println("通过beanPostProcessor接口的实现传入的String"+testBeanPostProcessor);
    }
}
