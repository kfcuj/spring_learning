package com.village.dog.Beans;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/*
   Bean中，方法执行顺序：
   1）执行构造方法
   2）执行依赖注入的方法
   3）执行PostConstruct（初始化）方法
   4) 执行销毁方法
 */
@Component
public class LifeCycleBean {
    private static final Logger log = LoggerFactory.getLogger(LifeCycleBean.class);
    //执行构造方法
    public LifeCycleBean(){log.info("构造");}
    //依赖注入方法
    @Autowired
    public void autowire(@Value("${Java_Home}") String home){
        log.info("依赖注入:{}",home);
    }
    // 初始化方法
    @PostConstruct
    public void init(){
        log.info("初始化");
    }
    // 销毁方法
    @PreDestroy
    public void destroy(){log.info("销毁");}
}
