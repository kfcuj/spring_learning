package com.village.dog.Beans;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.HashMap;

@SpringBootApplication
public class TestBeans {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(TestBeans.class,args);
        context.close();
    }
}
