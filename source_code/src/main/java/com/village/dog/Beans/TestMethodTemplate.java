package com.village.dog.Beans;

import java.util.ArrayList;
import java.util.List;

public class TestMethodTemplate {
    public static void main(String[] args) {
        MyBeanFactory beanFactory = new MyBeanFactory();
        BeanPostProcessor processor1 = bean -> System.out.println("解析@Autowird注解");
        BeanPostProcessor processor2 = bean -> System.out.println("解析@Component注解");
        beanFactory.addBeanPostProcessor(processor1);
        beanFactory.addBeanPostProcessor(processor2);
        beanFactory.getBean();
    }

    // Template Method Pattern
    /*
       背景：getBean方法可以拆解为几个方法的有序组合。为了提高getBean()方法扩展性
            可以引入模板方法为方法提供可扩展性，
            固定不变 <=> 方法执行过程中的主干
            变化的部分 <=> 抽象成接口，方便外部扩展
     */
    static class MyBeanFactory{
        public Object getBean(){
            Object bean = new Object();
            System.out.println("构造"+bean);
            System.out.println("依赖注入"+bean);
            // 通过模板方法让代码具有可扩展性，spring中通过模板方法
            for(BeanPostProcessor processor:processors){
                processor.inject(bean);
            }
            System.out.println("初始化"+bean);
            return bean;
        }

        private List<BeanPostProcessor> processors = new ArrayList<>();
        public void addBeanPostProcessor(BeanPostProcessor processor){
            processors.add(processor);
        }
    }


    // 模板方法首先需要一个接口
    interface BeanPostProcessor{
        void inject(Object bean);
    }
}
