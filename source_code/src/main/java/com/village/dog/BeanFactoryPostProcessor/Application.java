package com.village.dog.BeanFactoryPostProcessor;

import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.support.GenericApplicationContext;

@ComponentScan
public class Application {
    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean("config", Config.class);
        /* ConfigurationClassPostProcessor是Bean工厂后处理器:能够解析@ComponentScan,@Bean,@Import,@ImportResource注解  */
        context.registerBean(ConfigurationClassPostProcessor.class);
        /* MapperScannerConfigurer是Mybatis提供的bean工厂后处理器：能够扫描mybatis的@Mapper注解的类并注入到容器中,Mybatis提供
         的@MapperScan注解底层也是调用的该后置处理器。
         springboot整合通过自动配置用到，SSM整合会通过@MapperScanner注解*/
        context.registerBean(MapperScannerConfigurer.class,bd->{
            bd.getPropertyValues().add("basePackage","com.village.dog.BeanFactoryPostProcessor.mapper");
        });

        context.refresh();    // 启动容器
        for(String name:context.getBeanDefinitionNames()){ // 打印容器中的BeanDefinition
            System.out.println(name);
        }
        context.close();     // 销毁容器
    }
}
