package com.village.dog.BeanFactoryPostProcessor;

import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.stereotype.Component;
import java.io.IOException;
/*
   需求：自定义BeanFactory后置处理器用于解析@ComponentScan注解
 */
public class Application1 {
    public static void main(String[] args) throws IOException {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean("config", Config.class);

        /*
            方式1:自己定义法解析@ComponentScan，实现注解的容器类的beanDefinition添加到容器中
        */
        // addBeanDefinition(context);

        /*
            方式2:spring中通过模板方法提供BeanFactoryPostProcessor的扩展接口，
            因此可以自定义BeanFactoryPostProcessor，从而自己实现@ComponentScan注解的解析
         */
        context.registerBean(ComponentScanPostProcessor.class);

        context.refresh();    // 启动容器
        for(String name:context.getBeanDefinitionNames()){ // 打印容器中的BeanDefinition
            System.out.println(name);
        }
        context.close();     // 销毁容器
    }

    // 不足：下面实现中ComponentScan注解的类是写死的，实际Spring框架应该主动的扫描包从而获得所有拥有注解的class
    public static void addBeanDefinition(GenericApplicationContext context){
        try{
            ComponentScan componentScan = AnnotationUtils.findAnnotation(Config.class,ComponentScan.class);
            if(componentScan != null){

                for(String p:componentScan.basePackages()){     // 获取@ComponentScan的包名
                    // com.village.dog.BeanFactoryPostProcessor.component -> com/village/dog/BeanFactoryPostProcessor/component
                    // classpath*:com/village/dog/BeanFactoryPostProcessor/component/**/*.class
                    String path = "classpath*:" + p.replace(".","/") + "/**/*.class";
                    // 通过容器类加载特定path下的所有class字节码资源文件
                    Resource[] resources = context.getResources(path);
                    // 用于读取类字节码文件的类
                    CachingMetadataReaderFactory factory = new CachingMetadataReaderFactory();
                    // 生成BeanName的工具类
                    AnnotationBeanNameGenerator generator = new AnnotationBeanNameGenerator();

                    for(Resource r:resources){
                        // 通过类文件读取器获得注解的元数据
                        MetadataReader reader = factory.getMetadataReader(r);
                        AnnotationMetadata annotationMetadata = reader.getAnnotationMetadata();

                        System.out.println("类名"+reader.getClassMetadata().getClassName());
                        System.out.println("类上是否有@Component注解:" + reader.getAnnotationMetadata().hasAnnotation(Component.class.getName()));
                        System.out.println("类上是否有@Component的派生注解:" + reader.getAnnotationMetadata().hasMetaAnnotation(Component.class.getName()));

                        if(annotationMetadata.hasAnnotation(Component.class.getName()) || annotationMetadata.hasMetaAnnotation(Component.class.getName())){
                            // 基于类的元数据构建beanDefinition
                            AbstractBeanDefinition bd = BeanDefinitionBuilder.genericBeanDefinition(reader.getClassMetadata().getClassName()).getBeanDefinition();
                            DefaultListableBeanFactory beanFactory = context.getDefaultListableBeanFactory();
                            // 生成beanDefinition的名称
                            String beanName = generator.generateBeanName(bd,beanFactory);
                            // 在容器中注册beanDefinition
                            beanFactory.registerBeanDefinition(beanName,bd);
                        }
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
