package com.village.dog.BeanFactoryPostProcessor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.MethodMetadata;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.stereotype.Component;

import java.util.Set;

public class AtBeanPostProcessor implements BeanFactoryPostProcessor {

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        try{
              /*
             step1:读取配置类字节码文件，并获取@Bean注解的方法信息。
             注意:
                这种方式获得class并不会进行类加载，相较于类加载后通过反射获取class信息，这种方式效率更高。
         */
            CachingMetadataReaderFactory factory = new CachingMetadataReaderFactory();
            MetadataReader reader = factory.getMetadataReader(new ClassPathResource("com/village/dog/BeanFactoryPostProcessor/Config.class"));
            Set<MethodMetadata> methods = reader.getAnnotationMetadata().getAnnotatedMethods(Bean.class.getName());

        /*
           step2:获取工厂方法信息
         */
            for(MethodMetadata method:methods){
                System.out.println(method);
                String initMethodName = method.getAnnotationAttributes(Bean.class.getName()).get("initMethod").toString();
                BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition();
                // 第一个参数是工厂方法，第二参数是工厂方法所属的工厂Bean
                builder.setFactoryMethodOnBean(method.getMethodName(),"config");
                // 部分工厂方法以及构造方法需要参数，此时可以直接设置自动装配autowired，让框架帮忙设置bean方法参数
                builder.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_CONSTRUCTOR);
                if(initMethodName.length() > 0){
                    builder.setInitMethodName(initMethodName);
                }
                AbstractBeanDefinition bd = builder.getBeanDefinition();
                DefaultListableBeanFactory bf = (DefaultListableBeanFactory)beanFactory;
                bf.registerBeanDefinition(method.getMethodName(),bd);
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
