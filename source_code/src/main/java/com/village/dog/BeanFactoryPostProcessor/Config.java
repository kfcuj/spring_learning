package com.village.dog.BeanFactoryPostProcessor;

import com.alibaba.druid.pool.DruidDataSource;
import com.village.dog.BeanFactoryPostProcessor.mapper.Mapper1;
import com.village.dog.BeanFactoryPostProcessor.mapper.Mapper2;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.mapper.MapperFactoryBean;
import org.springframework.context.annotation.*;

import javax.sql.DataSource;
// 整合mybatis的相关类
@Configuration
@ImportResource
@ComponentScan("com.village.dog.BeanFactoryPostProcessor.component")
public class Config {
    @Bean
    public Bean1 bean1(){return new Bean1();}

    @Bean
    public SqlSessionFactoryBean sqlSessionFactoryBean(DataSource dataSource){
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource);
        return sqlSessionFactoryBean;
    }

    @Bean(initMethod = "init")
    public DruidDataSource dataSource() {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUrl("jdbc:mysql://localhost:3306/test");
        dataSource.setUsername("root");
        dataSource.setPassword("123456");
        return dataSource;
    }
    /*
       MyBatis底层也是通过下面这种方式将Mapper接口类添加到Spring容器中。
       不过为了能够批量进行添加，通过为spring提供工厂后置处理器扫描相关
       Mapper接口，并放入Spring容器中
     */
    // 该方法的参数也是通过容器注入
//    @Bean
//    public MapperFactoryBean<Mapper1> mapper1(SqlSessionFactory sqlSessionFactory){
//        MapperFactoryBean<Mapper1> factory = new MapperFactoryBean<>(Mapper1.class);
//        factory.setSqlSessionFactory(sqlSessionFactory);
//        return factory;
//    }
//
//    // 该方法的参数也是通过容器注入
//    @Bean
//    public MapperFactoryBean<Mapper2> mapper2(SqlSessionFactory sqlSessionFactory){
//        MapperFactoryBean<Mapper2> factory = new MapperFactoryBean<>(Mapper2.class);
//        factory.setSqlSessionFactory(sqlSessionFactory);
//        return factory;
//    }

}
