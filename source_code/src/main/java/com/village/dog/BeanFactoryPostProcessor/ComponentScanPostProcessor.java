package com.village.dog.BeanFactoryPostProcessor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.stereotype.Component;

public class ComponentScanPostProcessor implements BeanFactoryPostProcessor {

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        try{
            ComponentScan componentScan = AnnotationUtils.findAnnotation(Config.class,ComponentScan.class);
            if(componentScan != null){
                for(String p:componentScan.basePackages()){     // 获取@ComponentScan的包名
                    // com.village.dog.BeanFactoryPostProcessor.component -> com/village/dog/BeanFactoryPostProcessor/component
                    // classpath*:com/village/dog/BeanFactoryPostProcessor/component/**/*.class
                    String path = "classpath*:" + p.replace(".","/") + "/**/*.class";
                    // 加载特定path下的所有class字节码资源文件
                    Resource[] resources = new PathMatchingResourcePatternResolver().getResources(path);
                    // 用于读取类字节码文件的类
                    CachingMetadataReaderFactory factory = new CachingMetadataReaderFactory();
                    // 生成BeanName的工具类
                    AnnotationBeanNameGenerator generator = new AnnotationBeanNameGenerator();

                    for(Resource r:resources){
                        // 通过类文件读取器获得注解的元数据
                        MetadataReader reader = factory.getMetadataReader(r);
                        AnnotationMetadata annotationMetadata = reader.getAnnotationMetadata();

//                        System.out.println("类名"+reader.getClassMetadata().getClassName());
//                        System.out.println("类上是否有@Component注解:" + reader.getAnnotationMetadata().hasAnnotation(Component.class.getName()));
//                        System.out.println("类上是否有@Component的派生注解:" + reader.getAnnotationMetadata().hasMetaAnnotation(Component.class.getName()));

                        if(annotationMetadata.hasAnnotation(Component.class.getName()) || annotationMetadata.hasMetaAnnotation(Component.class.getName())){
                            // 基于类的元数据构建beanDefinition
                            AbstractBeanDefinition bd = BeanDefinitionBuilder.genericBeanDefinition(reader.getClassMetadata().getClassName()).getBeanDefinition();
                            // DefaultListableBeanFactory是ConfigurableListableBeanFactory接口的实现类，因此需要向下转型
                            DefaultListableBeanFactory bf = (DefaultListableBeanFactory)beanFactory;
                            // 生成beanDefinition的名称
                            String beanName = generator.generateBeanName(bd,bf);
                            // 在容器中注册beanDefinition是ConfigurableListableBeanFactory
                            bf.registerBeanDefinition(beanName,bd);
                        }
                    }
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
