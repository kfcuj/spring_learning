package com.village.dog.BeanFactoryPostProcessor;

import org.springframework.context.support.GenericApplicationContext;

import java.io.IOException;

public class Application3 {
    public static void main(String[] args) throws IOException {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean("config", Config.class);
        context.registerBean(ComponentScanPostProcessor.class);
        context.registerBean(AtBeanPostProcessor.class);
        // 该后置处理器用于处理@Mapper注解的接口类，放入到容器当中
        context.registerBean(MapperPostProcessor.class);

        context.refresh();    // 启动容器
        for(String name:context.getBeanDefinitionNames()){ // 打印容器中的BeanDefinition
            System.out.println(name);
        }
        context.close();     // 销毁容器
    }
}
