package com.village.dog.BeanFactoryPostProcessor;
import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.mapper.MapperFactoryBean;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;

/*/*
   BeanPostProcessor是高层接口，没有registerBeanDefinition方法
   而BeanDefinitionRegistryPostProcessor接口中有该方法。
   DefaultLisableBeanFactory是上述两个接口的实现类。
 **/
public class MapperPostProcessor implements BeanDefinitionRegistryPostProcessor {

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        BeanDefinitionRegistry beanFactory = registry;
        // 通过通配符资源解析器来加载所有的mapper接口的class文件
        try{
            PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
            Resource[] resources = resolver.getResources("classpath:com/village/" +
                    "dog/BeanFactoryPostProcessor/mapper/**/*.class");

            AnnotationBeanNameGenerator generator = new AnnotationBeanNameGenerator();
            CachingMetadataReaderFactory factory = new CachingMetadataReaderFactory();
            for(Resource resource:resources){
                MetadataReader reader = factory.getMetadataReader(resource);
                AnnotationMetadata annotationMetadata = reader.getAnnotationMetadata();
                ClassMetadata classMetadata = reader.getClassMetadata();
                if(classMetadata.isInterface() && annotationMetadata.hasAnnotation(Mapper.class.getName())){
                    AbstractBeanDefinition bd = BeanDefinitionBuilder.genericBeanDefinition(MapperFactoryBean.class)
                            .addConstructorArgValue(classMetadata.getClassName())
                            .setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_TYPE)   // 构造参数的自动装配
                            .getBeanDefinition();
                    /*
                          问题：Spring中如何对生成不同MapperFactoryBean<mapper>的beanName?
                              仅仅采用AnnotationBeanNameGenerator不能够对不同的mapper接口进行区分，原因在
                          于基于MapperFactoryBean<mapper>产生的beanName相同,由于容器采用key-value结构存储，beanName
                          相同会导致出现覆盖。

                          Spring的解决策略：对每个maper生成一个beanDefinition类，这个类不会注册到容器中，而是仅仅用于
                                   生成beanName,这样beanName就不会相同
                     */

                    // 此处生成的beanDefinition仅仅用于生成名称
                    AbstractBeanDefinition bd2 = BeanDefinitionBuilder
                            .genericBeanDefinition(classMetadata.getClassName())
                            .getBeanDefinition();
                    String beanName = generator.generateBeanName(bd2,beanFactory);
                    beanFactory.registerBeanDefinition(beanName,bd);




                }
            }




        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

    }
}
