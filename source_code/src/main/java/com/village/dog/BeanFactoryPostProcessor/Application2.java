package com.village.dog.BeanFactoryPostProcessor;

import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.MethodMetadata;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Set;

/*
   需求：自定义BeanFactory后置处理器用于解析配置类中@Bean注解的工厂方法
 */
public class Application2 {
    public static void main(String[] args) throws IOException {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean("config", Config.class);
        context.registerBean(ComponentScanPostProcessor.class);

        //beanPostProcessor(context);
        context.registerBean(AtBeanPostProcessor.class);

        context.refresh();    // 启动容器
        for(String name:context.getBeanDefinitionNames()){ // 打印容器中的BeanDefinition
            System.out.println(name);
        }
        context.close();     // 销毁容器
    }
    // 不足：下面@Configuration注解的类是写死的，实际Spring框架应该主动的扫描包从而获得所有拥有注解的class
    static void beanPostProcessor(GenericApplicationContext context) throws IOException {
          /*
             step1:读取配置类字节码文件，并获取@Bean注解的方法信息。
             注意:
                这种方式获得class并不会进行类加载，相较于类加载后通过反射获取class信息，这种方式效率更高。
         */
        CachingMetadataReaderFactory factory = new CachingMetadataReaderFactory();
        MetadataReader reader = factory.getMetadataReader(new ClassPathResource("com/village/dog/BeanFactoryPostProcessor/Config.class"));
        Set<MethodMetadata> methods = reader.getAnnotationMetadata().getAnnotatedMethods(Bean.class.getName());

        /*
           step2:获取工厂方法信息
         */
        for(MethodMetadata method:methods){
            System.out.println(method);
            String initMethodName = method.getAnnotationAttributes(Bean.class.getName()).get("initMethod").toString();
            BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition();
            // 第一个参数是工厂方法，第二参数是工厂方法所属的工厂Bean
            builder.setFactoryMethodOnBean(method.getMethodName(),"config");
            // 部分工厂方法以及构造方法需要参数，此时可以直接设置自动装配autowired，让框架帮忙设置bean方法参数
            builder.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_CONSTRUCTOR);
            if(initMethodName.length() > 0){
                builder.setInitMethodName(initMethodName);
            }
            AbstractBeanDefinition bd = builder.getBeanDefinition();
            context.getDefaultListableBeanFactory().registerBeanDefinition(method.getMethodName(),bd);
        }
    }
}
