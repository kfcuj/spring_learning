package com.village.dog.BeanFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.DefaultSingletonBeanRegistry;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.lang.reflect.Field;
import java.util.Map;

@SpringBootApplication
public class Application {
    private static final Logger logging = LoggerFactory.getLogger(Application.class);
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        ConfigurableApplicationContext context = SpringApplication.run(Application.class,args);

        /*
            需求：通过反射的方式获取存储单例对象的concurrentMap，打印处Map中我们自己注入的
                  两个单例对象Component1和Component2.
        */
        Field singletonObjects = DefaultSingletonBeanRegistry.class.getDeclaredField("singletonObjects");
        singletonObjects.setAccessible(true);
        // 获取现有的BeanFactory实现类
        ConfigurableListableBeanFactory beanFactory = context.getBeanFactory();
        // 获取该实现类的singletonObjects属性，也就是存放单例对象的map
        Map<String,Object> map = (Map<String, Object>) singletonObjects.get(beanFactory);
        map.entrySet().stream().filter(e->e.getKey().startsWith("component")).forEach(
                e-> System.out.println(e.getKey()+"="+e.getValue())
        );
    }
}