package com.village.dog.BeanFactory;

import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.boot.autoconfigure.web.servlet.DispatcherServletRegistrationBean;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.context.AnnotationConfigServletWebServerApplicationContext;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.mvc.Controller;

public class TestApplicationContext {
    public static void main(String[] args) {
        System.out.println("测试ClassPathXmlApplicationContext");
        testClassPathXmlApplicationContext();

        /*
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        // ==================ClassPathXmlApplication的内部机制流程=================
        printBeanDefinitionNames(beanFactory);
        XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(beanFactory);
        reader.loadBeanDefinitions(new ClassPathResource("b01.xml"));
        printBeanDefinitionNames(beanFactory);
        */
        System.out.println("测试AnnotationConfigApplicationContext");
        testAnnotationConfigApplicationContext();

        System.out.println("测试AnnotationConfigServletWebServerApplicationContext");
        testAnnotationConfigServletWebServerApplicationContext();

    }


    /*
            ClassPathXmlApplication:加载classpath路径下的xml进行配置
            FileSystemXmlApplicationContext:加载磁盘路径下的xml文件进行配置
     */
    private static void testClassPathXmlApplicationContext(){
        // FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext("xxx.xml");
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("b01.xml");
        printBeanDefinitionNames(context);
        System.out.println(context.getBean(Bean2.class).getBean1());
        System.out.println();

    }




    private static void testAnnotationConfigApplicationContext(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
        /*
           从输出可以看出：相较于ClassPathXmlApplication容器，该容器中除了配置类中配置的
           Bean1和Bean2,还有Config以及用于解析不同注解的5个工具类，作为bean的后处理器（如下所示）：
           org.springframework.context.annotation.internalConfigurationAnnotationProcessor
           org.springframework.context.annotation.internalAutowiredAnnotationProcessor
           org.springframework.context.annotation.internalCommonAnnotationProcessor
           org.springframework.context.event.internalEventListenerProcessor
           org.springframework.context.event.internalEventListenerFactory

           如果ClassPathXmlApplication加载的xml文件中包含<context:annotation-config/>，则也能引入上述5个后置处理器

         */
        printBeanDefinitionNames(context);
        System.out.println(context.getBean(Bean2.class).getBean1());
        System.out.println();
    }

    /*
      AnnotationConfigServletWebServerApplicationContext：
                            该容器也是基于Java配置类创建，主要用于web环境
     */
    private static void testAnnotationConfigServletWebServerApplicationContext(){

        // 内嵌Tomcat容器配合DispatchServlet实现简单的web应用
        // 最小系统： web容器，servlet对象，容器注册类，控制类处理请求
        AnnotationConfigServletWebServerApplicationContext  context = new AnnotationConfigServletWebServerApplicationContext (WebConfig.class);
        printBeanDefinitionNames(context);
        System.out.println();
    }

    @Configuration
    static class WebConfig{
        @Bean
        public ServletWebServerFactory servletWebServerFactory(){  // 创建内嵌Tomcat容器
            return new TomcatServletWebServerFactory();
        }

        @Bean
        public DispatcherServlet dispatcherServlet(){              // 创建Servlet对象
            return new DispatcherServlet();
        }

        // 将dispatchServlet注册到Tomcat容器中
        @Bean
        public DispatcherServletRegistrationBean registrationBean(DispatcherServlet dispatcherServlet){
            return new DispatcherServletRegistrationBean(dispatcherServlet,"/");
        }

        // 这里将/后面的bean名称作为访问路径
        @Bean("/hello")
        public Controller controller1(){
            return (request,response)->{
                    response.getWriter().print("Response Message:Hello");
                    return null;
                };
        }

    }

    @Configuration
    static class Config{
        @Bean
        public Bean1 bean1(){
            return new Bean1();
        }
        @Bean
        public Bean2 bean2(Bean1 bean1){
            Bean2 bean2 = new Bean2();
            bean2.setBean1(bean1);    // 注入依赖
            return bean2;
        }
    }

    static  class Bean1{}
    static  class Bean2{
        private Bean1 bean1;
        public void setBean1(Bean1 bean1){
            this.bean1 = bean1;
        }
        public Bean1 getBean1(){
            return bean1;
        }

    }

    private static void printBeanDefinitionNames(ListableBeanFactory beanFactory){
        System.out.println("==============beanDefinitions==================");
        for(String name:beanFactory.getBeanDefinitionNames()){
            System.out.println(name);
        }
        System.out.println("================================");
    }
}



