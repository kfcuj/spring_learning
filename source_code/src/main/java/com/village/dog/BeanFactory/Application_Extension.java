package com.village.dog.BeanFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.Resource;
import java.io.IOException;
import java.util.Locale;

@SpringBootApplication
public class Application_Extension {
    private static final Logger logging = LoggerFactory.getLogger(Application_Extension.class);
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException, IOException {
        ConfigurableApplicationContext context = SpringApplication.run(Application_Extension.class,args);
        /*
           1) ApplicationContext国际化功能:
           ========该继承于父接口MessageSource=====
           getMessage方法：同一key获取不同语言的value,从而实现国际化
           实际开发中，语言可以从浏览器的请求头获取
           注意：配置文件内容的编码方式需要设置的方式一致，IDEA中可以在setting中设置properties文件的编码方式为UTF8
         */
        System.out.println("1 国际化功能测试");
        System.out.println(context.getMessage("hi",null, Locale.CHINA));
        System.out.println(context.getMessage("hi",null,Locale.ENGLISH));
        System.out.println(context.getMessage("hi",null, Locale.JAPANESE));
        System.out.println();
        /*
           2) ApplicationContext:获取资源文件
           ========继承于于父接口MessageSource=====
         */
        System.out.println("2 获取资源测试");
        Resource[] resources = context.getResources("classpath:application.properties");
        for(Resource resource:resources){
            System.out.println(resource);
        }
        /*采用通配符获取jar包中名称为spring.factories的文件路径
          spring.factories的作用：https://zhuanlan.zhihu.com/p/444331676
         */
        Resource[] re = context.getResources("classpath*:META-INF/spring.factories");
        for(Resource resource:re){
            System.out.println(resource);
        }
        System.out.println();

        /*
            3） applicationContext的环境信息获取能力
            =============继承于父接口EnvironmentCapable====
         */
        // 配置信息获取：通过application获取windows系统环境变量Java_Home以及application.properties中的server.port属性
        System.out.println("3 环境信息获取测试");
        System.out.println(context.getEnvironment().getProperty("Java_Home"));
        System.out.println(context.getEnvironment().getProperty("server.port"));
        System.out.println();

        /*
           4) applicationContext的事件发布能力
           ==============继承于父接口ApplicationEventPublisher===
         */
        System.out.println("4 事件对象发布测试");
        context.publishEvent(new UserRegisterEvent(context));
    }
}