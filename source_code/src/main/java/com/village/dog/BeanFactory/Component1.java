package com.village.dog.BeanFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
// springboot默认是单例对象
@Component
public class Component1 {
    private static final Logger log = LoggerFactory.getLogger(Component1.class);

    /*
       通过事件分发器，可以实现业务上解耦：
       比如用户注册，后续操作可能有短信验证码，邮件验证码
       这个时候模块的功能的协同可以通过事件发布框架进行接口
     */
    @EventListener
    public void testMonitor(UserRegisterEvent event){
        log.info("收到发送的消息{}",event);
    }
}
