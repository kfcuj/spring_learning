package com.village.dog.BeanFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.annotation.AnnotationConfigUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;


public class TestBeanFactory {

    public static void main(String[] args) {
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        AbstractBeanDefinition beanDefinition =
        BeanDefinitionBuilder.genericBeanDefinition(Config.class).setScope("singleton").getBeanDefinition();
        beanFactory.registerBeanDefinition("config",beanDefinition);
        // 为容器添加处理器
        AnnotationConfigUtils.registerAnnotationConfigProcessors(beanFactory);
        // 执行BeanFactoryPostProcessor的逻辑
        beanFactory.getBeansOfType(BeanFactoryPostProcessor.class);
        beanFactory.getBeansOfType(BeanFactoryPostProcessor.class).values().forEach(beanFactoryPostProcessor ->{
            beanFactoryPostProcessor.postProcessBeanFactory(beanFactory);
        });
        beanFactory.getBeansOfType(BeanPostProcessor.class).values().forEach(beanFactory::addBeanPostProcessor);
//        beanFactory.getBeansOfType(BeanPostProcessor.class).values().forEach(beanPostProcessor->{
//            System.out.println("BeanFactory中的BeanPostProcessor:"+beanPostProcessor);
//            beanFactory.addBeanPostProcessor(beanPostProcessor);     // 先添加的processor先执行
//        });

        // 重新定义BeanPostProcessor的顺序
        beanFactory.getBeansOfType(BeanPostProcessor.class).values().stream().sorted(beanFactory.getDependencyComparator()).
                forEach(beanPostProcessor->{
            System.out.println("BeanFactory中的BeanPostProcessor:"+beanPostProcessor);
            beanFactory.addBeanPostProcessor(beanPostProcessor);     // 先添加的processor先执行
        });

        System.out.println(beanFactory.getBean(Bean1.class).getBean3());

   }

    @Configuration
    static class Config{
        @Bean
        public Bean1 bean1(){return new Bean1();}
        @Bean
        public Bean2 bean2(){return new Bean2();}
        @Bean
        public Bean3 bean3(){return new Bean3();}
        @Bean
        public Bean4 bean4(){return new Bean4();}

    }

    /*
       Bean1中Inter bean3，Inter接口有两个实现类
       Bean3,Bean4,
       情况1:
       @Autowired
       Inter bean3
       会根据bean3名称匹配到Bean3

       情况2:
        @Resource(name="bean4")
        Inter bean3;
        会根据name匹配到bean4

        上述如果两个注解一起用，都可以匹配，但最终匹配的是Bean3,原因是
        @Autowired对应的BeanPostProcessor在执行顺序上优于
        @Resource对应的BeanPostProcessor
        我们可以通过定义排序规则，让@Resource对应的BeanPostProcessor的执行顺序优于
        @Autowired对应的BeanPostProcessor
    */
    static class Bean1{
        private static final Logger log = LoggerFactory.getLogger(Bean1.class);
        public Bean1(){
            log.debug("构造Bean1()");
        }
        @Autowired
        private Bean2 bean2;
        public Bean2 getBean2(){return bean2;}
        @Autowired
        @Resource(name="bean4")
        Inter bean3;
        public Inter getBean3(){return bean3;}
    }

    static class Bean2{
        private static final Logger log = LoggerFactory.getLogger(Bean2.class);
        public Bean2(){
            log.debug("构造Bean2()");
        }
    }

    interface Inter{
        void get();
    }

    static class Bean3 implements Inter{
        @Override
        public void get() {}
    }

    static class Bean4 implements Inter{
        @Override
        public void get() {

        }
    }

}
