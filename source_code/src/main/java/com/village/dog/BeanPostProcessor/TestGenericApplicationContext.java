package com.village.dog.BeanPostProcessor;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.boot.context.properties.ConfigurationPropertiesBindingPostProcessor;
import org.springframework.context.annotation.CommonAnnotationBeanPostProcessor;
import org.springframework.context.annotation.ContextAnnotationAutowireCandidateResolver;
import org.springframework.context.support.GenericApplicationContext;

/*
   Bean的生命周期：构造方法=>依赖注入=>初始化=>销毁
   @Autowired,@Value,@Resource,@PostConstruct,@PreDestroy,@ConfigurationProperties注解解析时机：
  1)@Autowired和@Value和@Resource:在Bean生命周期的依赖注入（属性绑定）阶段
  2)@PostConstruct:在Bean生命周期的初始化阶段前
  3)@PreDestroy:在Bean的生命周期的销毁前
  4)@ConfigurationProperties:初始化前
 */
public class TestGenericApplicationContext {
    public static void main(String[] args) {
        // GenericApplication是一个"干净"的bean容器，所谓"干净"指的不会自动添加bean.
        GenericApplicationContext context = new GenericApplicationContext();

        context.registerBean("bean1", Bean1.class);
        context.registerBean("bean2", Bean2.class);
        context.registerBean("bean3", Bean3.class);
        context.registerBean("bean4",Bean4.class);

        /*
             初始化容器
             执行beanFactory后处理器，添加bean后处理器，初始化所有单例
         */
        //ContextAnnotationAutowireCandidateResolver支持@Value的值的解析
        context.getDefaultListableBeanFactory().setAutowireCandidateResolver(new ContextAnnotationAutowireCandidateResolver());
        // 1 添加后处理器用于解析@Autowird，@Value
        context.registerBean(AutowiredAnnotationBeanPostProcessor.class);
        // 2 添加后处理器用于解析@Resource,@PostConstruct @PreDestroy
        context.registerBean(CommonAnnotationBeanPostProcessor.class);
        // 3 添加后处理器用于解析@ConfigurationProperties注解,实现键值信息的注入
        ConfigurationPropertiesBindingPostProcessor.register(context.getDefaultListableBeanFactory());
        context.refresh();
        System.out.println(context.getBean(Bean4.class).toString());
        // 销毁容器
        context.close();
    }
}
