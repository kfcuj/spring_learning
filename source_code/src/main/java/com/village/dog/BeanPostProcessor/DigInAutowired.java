package com.village.dog.BeanPostProcessor;

import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.config.DependencyDescriptor;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.annotation.ContextAnnotationAutowireCandidateResolver;
import org.springframework.core.MethodParameter;
import org.springframework.core.env.StandardEnvironment;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

// 分析AutowiredAnnotationBeanPostProcessor的执行过程
public class DigInAutowired {
    public static void main(String[] args) throws Throwable {
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        // 采用registerSingleton方法将bean实例放入容器中，不会有实例构造，属性注入，初始化的过程
        beanFactory.registerSingleton("bean2",new Bean2());
        beanFactory.registerSingleton("bean3",new Bean3());
        beanFactory.registerSingleton("bean5",new Bean5());
        beanFactory.setAutowireCandidateResolver(new ContextAnnotationAutowireCandidateResolver());
        // Java8方法引用: https://www.cnblogs.com/zhixie/p/13297141.html
        // 这里添加的方法用于解析注解中的 ${} 表达式
        beanFactory.addEmbeddedValueResolver(new StandardEnvironment()::resolvePlaceholders);
        AutowiredAnnotationBeanPostProcessor processor = new AutowiredAnnotationBeanPostProcessor();
        processor.setBeanFactory(beanFactory);

        /*
            问题：spring如果根据类型匹配容器中的bean,基本流程如何？
         */
        // 情况1: @Autowired注解的成员属性
         Field  bean5 = Bean1.class.getDeclaredField("bean5");
         // DependencyDescriptor是专门用于描述注入的依赖的类，包含构造方法，方法参数和属性
         DependencyDescriptor dependencyDescriptor = new DependencyDescriptor(bean5,false);
         // doResolveDependency基于依赖信息从容器中匹配适合的实例赋值给对应依赖
         Object o = beanFactory.doResolveDependency(dependencyDescriptor,null,null,null);
         System.out.println(o);

        // 情况2:@Autowired注解的方法参数
        Method setBean2 = Bean1.class.getDeclaredMethod("setBean2",Bean2.class);
        DependencyDescriptor dep = new DependencyDescriptor(new MethodParameter(setBean2,0),false);
        o = beanFactory.doResolveDependency(dep,null,null,null);
        System.out.println(o);

        // 情况3:@Value注入的值(String)的匹配过程
        Method setHome = Bean1.class.getDeclaredMethod("setHome",String.class);
        DependencyDescriptor dep3 = new DependencyDescriptor(new MethodParameter(setHome,0),true);
        o = beanFactory.doResolveDependency(dep3,null,null,null);
        System.out.println(o);
    }
}
