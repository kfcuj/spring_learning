### 1 bean的生命周期概述
#### 1-1 生命周期相关接口介绍
BeanFactory接口：

#### 1-2 生命周期测试代码

需求：Spring中通过Bean的后置处理程序（PostProcess）实现对Bean的创建过程扩展，下面代码通过自定义的
BeanPostProcessor展示bean的生命流程。
* 定义被观察bean代码
```java
package com.village.dog.Beans;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/*
   Bean中，方法执行顺序：
   1）执行构造方法
   2）执行依赖注入的方法
   3）执行PostConstruct（初始化）方法
   4) 执行销毁方法

 */
@Component
public class LifeCycleBean {
    private static final Logger log = LoggerFactory.getLogger(LifeCycleBean.class);
    //执行构造方法
    public LifeCycleBean(){log.info("构造");}
    //依赖注入方法
    @Autowired
    public void autowire(@Value("${Java_Home}") String home){
        log.info("依赖注入:{}",home);
    }
    // 初始化方法
    @PostConstruct
    public void init(){
        log.info("初始化");
    }
    // 销毁方法
    @PreDestroy
    public void destroy(){log.info("销毁");}
}
```
* 自定义Bean后置处理器监控bean创建流程的代码
```java
package com.village.dog.Beans;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.config.DestructionAwareBeanPostProcessor;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.stereotype.Component;

/*
   bean的生命周期：
    1）创建（实例化）
    2) 依赖注入
    3) 初始化
    4）销毁
 */

@Component
public class MyBeanPostProcessor implements InstantiationAwareBeanPostProcessor, DestructionAwareBeanPostProcessor {
    private static final Logger log = LoggerFactory.getLogger(MyBeanPostProcessor.class);
    // instantiation  [ɪnstænʃɪ'eɪʃən]  实例化，构造函数调用即标志对象实例化
    @Override
    public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
        if(beanName.equals("lifeCycleBean"))
            log.info("实例化前执行，返回的对象替代原本的bean");
        return null;      // 返回null则保持原有对象不变
    }


    @Override
    public boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException {
        if(beanName.equals("lifeCycleBean"))
            log.info("实例化后执行，返回false会跳过依赖注入阶段");
        return true;
    }

    @Override
    public PropertyValues postProcessProperties(PropertyValues pvs, Object bean, String beanName) throws BeansException {
        if(beanName.equals("lifeCycleBean"))
            log.info("依赖注入阶段执行，包括@Autowired、@Value、@Resource");
        return pvs;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if(beanName.equals("lifeCycleBean"))
            log.info("初始化之前执行，返回对象会替代原本的bean,如@PostConstruct、@ConfigurationProperties");
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if(beanName.equals("lifeCycleBean"))
            log.info("初始化后执行，返回对象会替代原本bean,如代理增强");
        return bean;
    }

    @Override
    public void postProcessBeforeDestruction(Object bean, String beanName) throws BeansException {
        if(beanName.equals("lifeCycleBean"))
            log.info("Bean销毁之前执行,调用时机于@PreDestroy注解的方法相同");
    }
}

```

* 启动/关闭Spring容器代码
```java
package com.village.dog.Beans;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class TestBeans {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(TestBeans.class,args);
        context.close();
    }
}

```
* 日志输出
```java
[2022-06-02 23:05:49] [INFO ] -- Starting TestBeans using Java 1.8.0_131 on DESKTOP-V3MT772 with PID 13100 (H:\spring基础\source_code\target\classes started by Administrator in H:\spring基础\source_code)
[2022-06-02 23:05:49] [INFO ] -- No active profile set, falling back to default profiles: default
[2022-06-02 23:05:52] [INFO ] -- Tomcat initialized with port(s): 9006 (http)
[2022-06-02 23:05:52] [INFO ] -- Initializing ProtocolHandler ["http-nio-9006"]
[2022-06-02 23:05:52] [INFO ] -- Starting service [Tomcat]
[2022-06-02 23:05:52] [INFO ] -- Starting Servlet engine: [Apache Tomcat/9.0.53]
[2022-06-02 23:05:52] [INFO ] -- Initializing Spring embedded WebApplicationContext
[2022-06-02 23:05:52] [INFO ] -- Root WebApplicationContext: initialization completed in 2832 ms
[2022-06-02 23:05:52] [INFO ] -- 实例化前执行，返回的对象替代原本的bean
[2022-06-02 23:05:52] [INFO ] -- 构造
[2022-06-02 23:05:52] [INFO ] -- 实例化后执行，返回false会跳过依赖注入阶段
[2022-06-02 23:05:52] [INFO ] -- 依赖注入阶段执行，包括@Autowired、@Value、@Resource
[2022-06-02 23:05:52] [INFO ] -- 依赖注入:C:\Program Files\Java\jdk1.8.0_131
[2022-06-02 23:05:52] [INFO ] -- 初始化之前执行，返回对象会替代原本的bean,如@PostConstruct、@ConfigurationProperties
[2022-06-02 23:05:52] [INFO ] -- 初始化
[2022-06-02 23:05:52] [INFO ] -- 初始化后执行，返回对象会替代原本bean,如代理增强
[2022-06-02 23:05:52] [INFO ] -- Starting ProtocolHandler ["http-nio-9006"]
[2022-06-02 23:05:52] [INFO ] -- Tomcat started on port(s): 9006 (http) with context path ''
[2022-06-02 23:05:52] [INFO ] -- Started TestBeans in 4.498 seconds (JVM running for 7.926)
[2022-06-02 23:05:54] [INFO ] -- Bean销毁之前执行,调用时机于@PreDestroy注解的方法相同
[2022-06-02 23:05:54] [INFO ] -- 销毁
```
***
##### 1-3 生命周期总结
上述代码中通过定义的MyBeanProcessor，展示LifeCycleBean在Spring容器中执行过程。其中
MyBeanProcessor的关系图如下所示

![image-20220512223312618](BeanPostProcessor.jpg)

<center>图2：Diagram for BeanPostProcessor</center>

父接口的定义如下所示：
```java
public interface BeanPostProcessor {
	@Nullable
	default Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}
	@Nullable
	default Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}

}

public interface InstantiationAwareBeanPostProcessor extends BeanPostProcessor {
	@Nullable
	default Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
		return null;
	}
	default boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException {
		return true;
	}
	@Nullable
	default PropertyValues postProcessProperties(PropertyValues pvs, Object bean, String beanName)
			throws BeansException {

		return null;
	}
	@Deprecated
	@Nullable
	default PropertyValues postProcessPropertyValues(
			PropertyValues pvs, PropertyDescriptor[] pds, Object bean, String beanName) throws BeansException {

		return pvs;
	}
}
public interface DestructionAwareBeanPostProcessor extends BeanPostProcessor {
	void postProcessBeforeDestruction(Object bean, String beanName) throws BeansException;
	default boolean requiresDestruction(Object bean) {
		return true;
	}
}

```
接口方法主要提供了以下5个阶段的扩展：BeforeInstantiation，AfterInstantiation，
Properties，BeforeInitialization，AfterInitialization，BeforeDestruction
* instantiation[ɪnstænʃɪ'eɪʃən]:实例化
* Initialization[ɪˌnɪʃəlaɪ'zeɪʃn]：设定初值，初始化
  
因此Spring bean的生命周期主要四个阶段： 实例化，属性注入，初始化，销毁。Spring容器通过模板方法模式，为每个阶段抽象出方法，从而实现Bean生命周期各个阶段的扩展。
##### 知识点：bean生命周期中模板方法模式
**模板方法**
* 使用动机：为代码提供扩展性，模板方法的使用符合开闭原则
* 使用时机：设计时，有部分内容是确定的，还有部分内容并不能完全确定，需要具有扩展性，对于不确定的逻辑抽象为接口，方便后续修改。
模板方法代码示例：
```java
package com.village.dog.Beans;

import java.util.ArrayList;
import java.util.List;

public class TestMethodTemplate {
    public static void main(String[] args) {
        MyBeanFactory beanFactory = new MyBeanFactory();
        BeanPostProcessor processor1 = bean -> System.out.println("解析@Autowird注解");
        BeanPostProcessor processor2 = bean -> System.out.println("解析@Component注解");
        beanFactory.addBeanPostProcessor(processor1);
        beanFactory.addBeanPostProcessor(processor2);
        beanFactory.getBean();
    }

    // Template Method Pattern
    /*
       背景：getBean方法可以拆解为几个方法的有序组合。为了提高getBean()方法扩展性
            可以引入模板方法为方法提供可扩展性，
            固定不变 <=> 方法执行过程中的主干
            变化的部分 <=> 抽象成接口，方便外部扩展
     */
    static class MyBeanFactory{
        public Object getBean(){
            Object bean = new Object();
            System.out.println("构造"+bean);
            System.out.println("依赖注入"+bean);
            // 通过模板方法让代码具有可扩展性，spring中通过模板方法
            for(BeanPostProcessor processor:processors){
                processor.inject(bean);
            }
            System.out.println("初始化"+bean);
            return bean;
        }

        private List<BeanPostProcessor> processors = new ArrayList<>();
        public void addBeanPostProcessor(BeanPostProcessor processor){
            processors.add(processor);
        }
    }
    // 模板方法首先需要一个接口
    interface BeanPostProcessor{
        void inject(Object bean);
    }
}

```
上述代码中将不确定的部分抽线为接口方法，体现了模板方法思想。比如注解的解析，如果有新的注解出现，只需要添加新的注解实现类即可，从而避免对原始的类加载骨干代码进行修改。

### 二 bean后处理器


#### 2-1 常见后处理的功能和执行时机

**功能测试代码**
  
* 四个Bean定义
```java
package com.village.dog.postProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

public class Bean1 {
    private static final Logger log = LoggerFactory.getLogger(Bean1.class);
    private Bean2 bean2;
    @Autowired
    public void setBean2(Bean2 bean){
        log.info("@Autowired 生效：{}",bean);
        this.bean2 = bean;
    }
    private Bean3 bean3;
    @Resource
    public void setBean3(Bean3 bean){
        log.info("@Resource生效：{}",bean);
        this.bean3 = bean;
    }
    private String home;
    // 读取Java Home目录路径作为值
    @Autowired
    public void setHome(@Value("${Java_Home}") String home){
        this.home = home;
        log.info("@Value 生效：{}",home);
    }
    @PostConstruct
    public void init(){
        log.info("@PostConstruct 生效");
    }
    @PreDestroy
    public void destroy(){
        log.info("@PreDestroy 生效");
    }
    @Override
    public String toString() {
        return "Bean1{" +
                "bean2=" + bean2 +
                ", bean3=" + bean3 +
                ", home='" + home + '\'' +
                '}';
    }
}
```
```java
package com.village.dog.postProcessor;
public class Bean2 {
}
```

```java
package com.village.dog.postProcessor;
public class Bean3 {
}
```

```java
package com.village.dog.postProcessor;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix= "java")
public class Bean4 {
    private String home;
    private String version;
    @Override
    public String toString() {
        return "Bean4{" +
                "home='" + home + '\'' +
                ", version='" + version + '\'' +
                '}';
    }
    public String getHome() {
        return home;
    }
    public void setHome(String home) {
        this.home = home;
    }
    public String getVersion() {
        return version;
    }
    public void setVersion(String version) {
        this.version = version;
    }
}
```
* 定义的Bean注入容器代码
```java
package com.village.dog.postProcessor;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.boot.context.properties.ConfigurationPropertiesBindingPostProcessor;
import org.springframework.context.annotation.CommonAnnotationBeanPostProcessor;
import org.springframework.context.annotation.ContextAnnotationAutowireCandidateResolver;
import org.springframework.context.support.GenericApplicationContext;

/*
   Bean的生命周期：构造方法=>依赖注入=>初始化=>销毁
   @Autowired,@Value,@Resource,@PostConstruct,@PreDestroy,@ConfigurationProperties注解解析时机：
  1)@Autowired和@Value和@Resource:在Bean生命周期的依赖注入（属性绑定）阶段
  2)@PostConstruct:在Bean生命周期的初始化阶段前
  3)@PreDestroy:在Bean的生命周期的销毁前
  4)@ConfigurationProperties:初始化前
 */
public class TestGenericApplicationContext {
    public static void main(String[] args) {
        // GenericApplication是一个"干净"的bean容器，所谓"干净"指的不会自动添加bean.
        GenericApplicationContext context = new GenericApplicationContext();

        context.registerBean("bean1", Bean1.class);
        context.registerBean("bean2", Bean2.class);
        context.registerBean("bean3", Bean3.class);
        context.registerBean("bean4",Bean4.class);

        /*
             初始化容器
             执行beanFactory后处理器，添加bean后处理器，初始化所有单例
         */
        //ContextAnnotationAutowireCandidateResolver支持@Value的值的解析
        context.getDefaultListableBeanFactory().setAutowireCandidateResolver(new ContextAnnotationAutowireCandidateResolver());
        // 1 添加后处理器用于解析@Autowird，@Value
        context.registerBean(AutowiredAnnotationBeanPostProcessor.class);
        // 2 添加后处理器用于解析@Resource,@PostConstruct @PreDestroy
        context.registerBean(CommonAnnotationBeanPostProcessor.class);
        // 3 添加后处理器用于解析@ConfigurationProperties注解,实现键值信息的注入
        ConfigurationPropertiesBindingPostProcessor.register(context.getDefaultListableBeanFactory());
        context.refresh();
        System.out.println(context.getBean(Bean4.class).toString());
        // 销毁容器
        context.close();
    }
}
```
* 执行结果
```java
[2022-06-15 14:16:14] [DEBUG] -- Refreshing org.springframework.context.support.GenericApplicationContext@b97c004
[2022-06-15 14:16:14] [DEBUG] -- Creating shared instance of singleton bean 'org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor'
[2022-06-15 14:16:14] [DEBUG] -- Creating shared instance of singleton bean 'org.springframework.context.annotation.CommonAnnotationBeanPostProcessor'
[2022-06-15 14:16:14] [DEBUG] -- Creating shared instance of singleton bean 'org.springframework.boot.context.properties.ConfigurationPropertiesBindingPostProcessor'
[2022-06-15 14:16:14] [DEBUG] -- Creating shared instance of singleton bean 'org.springframework.boot.context.internalConfigurationPropertiesBinder'
[2022-06-15 14:16:14] [DEBUG] -- Creating shared instance of singleton bean 'org.springframework.boot.context.internalConfigurationPropertiesBinderFactory'
[2022-06-15 14:16:14] [DEBUG] -- Creating shared instance of singleton bean 'bean1'
[2022-06-15 14:16:14] [DEBUG] -- Creating shared instance of singleton bean 'bean3'
[2022-06-15 14:16:14] [INFO ] -- @Resource生效：com.village.dog.postProcessor.Bean3@5db45159
[2022-06-15 14:16:14] [DEBUG] -- Found key 'Java_Home' in PropertySource 'systemEnvironment' with value of type String
[2022-06-15 14:16:14] [INFO ] -- @Value 生效：C:\Program Files\Java\jdk1.8.0_131
[2022-06-15 14:16:14] [DEBUG] -- Creating shared instance of singleton bean 'bean2'
[2022-06-15 14:16:14] [INFO ] -- @Autowired 生效：com.village.dog.postProcessor.Bean2@11a9e7c8
[2022-06-15 14:16:14] [INFO ] -- @PostConstruct 生效
[2022-06-15 14:16:14] [DEBUG] -- Creating shared instance of singleton bean 'bean4'
Bean4{home='C:\Program Files\Java\jdk1.8.0_131\jre', version='1.8.0_131'}
[2022-06-15 14:16:14] [DEBUG] -- Closing org.springframework.context.support.GenericApplicationContext@b97c004, started on Wed Jun 15 14:16:14 CST 2022
[2022-06-15 14:16:14] [INFO ] -- @PreDestroy 生效
```
**总结：** 上述测试代码表明基于注解的bean的注入是通过一系列的postProcessor完成的。官方框架本身提供了不同的postProcessor可以用解析不同的注解。此外由于采用模板方法，**用户本身也可以自定义注解以及所对应的PostProcessor，从而能够灵活实现bean加载不同阶段的扩展。这种基本模板方法的扩展思想值得学习**。



#### 2-2 AutowiredAnnotationBeanPostProcessor流程解析
**功能测试代码**
```java
package com.village.dog.postProcessor;

import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.annotation.ContextAnnotationAutowireCandidateResolver;

// 分析AutowiredAnnotationBeanPostProcessor的执行过程
public class DigInAutowired {
    public static void main(String[] args) {
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();

        // 采用registerSingleton方法将bean实例放入容器中，不会有实例构造，属性注入，初始化的过程
        beanFactory.registerSingleton("bean2",new Bean2());
        beanFactory.registerSingleton("bean3",new Bean3());
        beanFactory.setAutowireCandidateResolver(new ContextAnnotationAutowireCandidateResolver());

        // 1. 查看哪些属性，方法加了@Autowired,称之为InjectionMetadata
        AutowiredAnnotationBeanPostProcessor processor = new AutowiredAnnotationBeanPostProcessor();
        processor.setBeanFactory(beanFactory);

        Bean1 bean1 = new Bean1();    // 实例化bean1，此时的bean1的依赖都没有注入
        System.out.println(bean1);    // 打印未进行依赖注入的bean
        // 调用postProcessorProperties后解析了@Autowird和@Value注解为bean1注入依赖
        processor.postProcessProperties(null,bean1,"bean1");
        System.out.println(bean1);    // 打印依赖注入后的bean
    }
}
```
**执行结果**
```java
Bean1{bean2=null, bean3=null, home='null'}
[2022-06-15 14:41:57] [INFO ] -- @Autowired 生效：com.village.dog.postProcessor.Bean2@6fb0d3ed
[2022-06-15 14:41:57] [INFO ] -- @Value 生效：${Java_Home}
Bean1{bean2=com.village.dog.postProcessor.Bean2@6fb0d3ed, bean3=null, home='${Java_Home}'}
```
**AutowiredAnnotationBeanPostProcessor中postProcessProperties方法源码**
```java
	@Override
	public PropertyValues postProcessProperties(PropertyValues pvs, Object bean, String beanName) {
        // 寻找当前bean中有@Autowired注解的成员变量和方法参数，并将找到的信息封装到metadata中
		InjectionMetadata metadata = findAutowiringMetadata(beanName, bean.getClass(), pvs);
		try {
            // 为成员变量和方法参数进行属性注入。
			metadata.inject(bean, beanName, pvs);
		}
		catch (BeanCreationException ex) {
			throw ex;
		}
		catch (Throwable ex) {
			throw new BeanCreationException(beanName, "Injection of autowired dependencies failed", ex);
		}
		return pvs;
	}

```
**总结：** 依赖注入的过程可进一步拆解为 findAutowiringMetadata和metadata.inject(bean, beanName, pvs)这两个步骤，步骤1用于找到添加注解的成员属性和方法参数封装成元信息。步骤2，基于成员属性和方法参数的类型进行依赖赋值。
***
**拆解步骤功能测试**

**目标：** 总体把握@Autowired注解解析的基本步骤

```java
package com.village.dog.postProcessor;

import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.annotation.InjectionMetadata;
import org.springframework.beans.factory.config.DependencyDescriptor;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.annotation.ContextAnnotationAutowireCandidateResolver;
import org.springframework.core.env.StandardEnvironment;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLOutput;

// 分析AutowiredAnnotationBeanPostProcessor的执行过程
public class DigInAutowired {
    public static void main(String[] args) throws Throwable {
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();

        // 采用registerSingleton方法将bean实例放入容器中，不会有实例构造，属性注入，初始化的过程
        beanFactory.registerSingleton("bean2",new Bean2());
        beanFactory.registerSingleton("bean3",new Bean3());
        beanFactory.registerSingleton("bean5",new Bean5());

        beanFactory.setAutowireCandidateResolver(new ContextAnnotationAutowireCandidateResolver());
        // Java8方法引用: https://www.cnblogs.com/zhixie/p/13297141.html
        // 这里添加的方法用于解析 ${} 表达式
        beanFactory.addEmbeddedValueResolver(new StandardEnvironment()::resolvePlaceholders);

        AutowiredAnnotationBeanPostProcessor processor = new AutowiredAnnotationBeanPostProcessor();
        processor.setBeanFactory(beanFactory);

        Bean1 bean1 = new Bean1();
        System.out.println(bean1.toString());

        /*
           processor.postProcessProperties(null,bean1,"bean1");
           postProcessProperties中@Autowired的注解解析拆分为两个步骤
        */
        // 步骤1:找到bean中添加@Autowird注解的成员变量和方法参数的信息
        Method findAutowiringMetadata = AutowiredAnnotationBeanPostProcessor.class.getDeclaredMethod("findAutowiringMetadata", String.class, Class.class, PropertyValues.class);
        findAutowiringMetadata.setAccessible(true);   // 该方法是私有方法，通过反射强行调用
        InjectionMetadata metadata = (InjectionMetadata)findAutowiringMetadata.invoke(processor,"bean1",bean1.getClass(),null);
        System.out.println(metadata);
        // 步骤2: 根据成员变量和方法参数类型注入依赖
        metadata.inject(bean1,"bean1",null);
        System.out.println(bean1.toString());
    }
}
```
测试结果
```java
Bean1{bean2=null, bean3=null, home='null'}
org.springframework.beans.factory.annotation.InjectionMetadata@27d415d9
[2022-06-15 16:44:59] [INFO ] -- @Autowired 生效：com.village.dog.postProcessor.Bean2@69379752
[2022-06-15 16:44:59] [DEBUG] -- Found key 'Java_Home' in PropertySource 'systemEnvironment' with value of type String
[2022-06-15 16:44:59] [INFO ] -- @Value 生效：C:\Program Files\Java\jdk1.8.0_131
Bean1{bean2=com.village.dog.postProcessor.Bean2@69379752, bean3=null, home='C:\Program Files\Java\jdk1.8.0_131'}
```
**总结** ：上述代码，首先通过反射调用后置处理器的getDeclaredMethod方法，该方法能够寻找添加@Autowired注解的成员变量和方法参数并封装到InjectionMetadata对象中，然后调用InjectionMetadata对象的inject方法，该方法基于类型从容器中匹配实例对象并赋值给添加注解的方法参数或成员变量。

* 如图2所示，通过断点调试可以从InjectionMetadata中发现@Autowired注解的成员变量和方法参数信息，

![image-20220512223312618](AutoWiredMetaData.jpg)
<center>图2：通过Autowired注解获取的元信息实例</center>

***

**问题：如果根据InjectionMetaData的类型信息从容器中匹配到具体的实例？**

**前置知识**
* [Spring中依赖注入的相关注解](https://www.cnblogs.com/kfcuj/p/14929510.html#5-3-%E4%BE%9D%E8%B5%96%E6%B3%A8%E5%85%A5%E7%9B%B8%E5%85%B3%E7%9A%84%E6%B3%A8%E8%A7%A3)
* [Autowired误用引发的问题](https://www.bilibili.com/video/BV1HF411c7GX?spm_id_from=333.999.0.0&vd_source=851f63d82a2e80a24b863617bae15845)
  

```java
package com.village.dog.postProcessor;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.annotation.InjectionMetadata;
import org.springframework.beans.factory.config.DependencyDescriptor;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.annotation.ContextAnnotationAutowireCandidateResolver;
import org.springframework.core.MethodParameter;
import org.springframework.core.env.StandardEnvironment;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLOutput;

// 分析AutowiredAnnotationBeanPostProcessor的执行过程
public class DigInAutowired {
    public static void main(String[] args) throws Throwable {
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        // 采用registerSingleton方法将bean实例放入容器中，不会有实例构造，属性注入，初始化的过程
        beanFactory.registerSingleton("bean2",new Bean2());
        beanFactory.registerSingleton("bean3",new Bean3());
        beanFactory.registerSingleton("bean5",new Bean5());
        beanFactory.setAutowireCandidateResolver(new ContextAnnotationAutowireCandidateResolver());
        // Java8方法引用: https://www.cnblogs.com/zhixie/p/13297141.html
        // 这里添加的方法用于解析注解中的 ${} 表达式
        beanFactory.addEmbeddedValueResolver(new StandardEnvironment()::resolvePlaceholders);
        AutowiredAnnotationBeanPostProcessor processor = new AutowiredAnnotationBeanPostProcessor();
        processor.setBeanFactory(beanFactory);

        /*
            问题：spring如果根据类型匹配容器中的bean,基本流程如何？
         */
        // 情况1: @Autowired注解的成员属性
         Field  bean5 = Bean1.class.getDeclaredField("bean5");
         // DependencyDescriptor是专门用于描述注入的依赖的类，包含构造方法，方法参数和属性
         DependencyDescriptor dependencyDescriptor = new DependencyDescriptor(bean5,false);
         // doResolveDependency基于依赖信息从容器中匹配适合的实例赋值给对应依赖
         Object o = beanFactory.doResolveDependency(dependencyDescriptor,null,null,null);
         System.out.println(o);

        // 情况2:@Autowired注解的方法参数
        Method setBean2 = Bean1.class.getDeclaredMethod("setBean2",Bean2.class);
        DependencyDescriptor dep = new DependencyDescriptor(new MethodParameter(setBean2,0),false);
        o = beanFactory.doResolveDependency(dep,null,null,null);
        System.out.println(o);

        // 情况3:@Value注入的值(String)的匹配过程
        Method setHome = Bean1.class.getDeclaredMethod("setHome",String.class);
         dep3 = new DependencyDescriptor(new MethodParameter(setHome,0),true);
        o = beanFactory.doResolveDependency(dep3,null,null,null);
        System.out.println(o);
    }
}
```
总结： 通过@Autowired或者@Value注入的依赖的基本过程都是相似度，首先将依赖封装到DependencyDescriptor对象中，该对象能够设置
无法匹配时是否抛出noBeanDefinition的异常，然后调用Bean工厂的doResolveDependency方法，该方法会基于DependencyDescriptor获取实例对象。

### 三 BeanFactory的后处理器

* 作用：BeanFactory的扩展,注意区别Bean的后处理器

#### 3-1 两种BeanFactory后处理器介绍

实际开发中，两个BeanFactory后处理器ConfigurationClassPostProcessor和MapperScannerConfigurer经常会被使用，
虽然作为框架使用者，我们并不会直接调用类的方法。当我们采用@ComponentScan,@Bean,@Import,@ImportResource
注解容器类，框架会调用ConfigurationClassPostProcessor处理注解，而MapperScannerConfigurer则是由mybatis框架提供处理器用于解析
@MapperScan注解。这里可以发现模板方法的好处，提供了必要的可扩展性。不同的外部组件可以提供不同的后置处理器供容器调用从而处理该组件特定的注解。

```
@ComponentScan:Configures component scanning directives for use with Configuration classes(定义包扫描的规则).

@Configuration:Indicates that a class declares one or more @Bean methods and
 may be processed by the Spring container to generate bean definitions and
 service requests for those beans at runtime(配置类中包含@Bean注解的成员方法)

@Import:Indicates one or more component classes to import; typically @Configuration classes.

@ImportResource:Indicates one or more resources containing bean definitions to import.
```


```java
package com.village.dog.BeanFactoryPostProcessor;

import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.support.GenericApplicationContext;

public class Application {
    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean("config", Config.class);
        /* ConfigurationClassPostProcessor是Bean工厂后处理器:能够解析@ComponentScan,@Bean,@Import,@ImportResource注解  */
        context.registerBean(ConfigurationClassPostProcessor.class);
        /* MapperScannerConfigurer是Mybatis提供的bean工厂后处理器：能够扫描mybatis的@Mapper注解的类并注入到容器中,Mybatis提供
         的@MapperScan注解底层也是调用的该后置处理器。
         springboot整合通过自动配置用到，SSM整合会通过@MapperScanner注解*/
        context.registerBean(MapperScannerConfigurer.class,bd->{
            bd.getPropertyValues().add("basePackage","com.village.dog.BeanFactoryPostProcessor.mapper");
        });

        context.refresh();    // 启动容器
        for(String name:context.getBeanDefinitionNames()){ // 打印容器中的BeanDefinition
            System.out.println(name);
        }
        context.close();     // 销毁容器
    }
}
```
**执行结果**
```java
config
org.springframework.context.annotation.ConfigurationClassPostProcessor
org.mybatis.spring.mapper.MapperScannerConfigurer
bean2
bean1
sqlSessionFactoryBean
dataSource
mapper1
mapper2
org.springframework.context.annotation.internalConfigurationAnnotationProcessor
org.springframework.context.annotation.internalAutowiredAnnotationProcessor
org.springframework.context.annotation.internalCommonAnnotationProcessor
org.springframework.context.event.internalEventListenerProcessor
org.springframework.context.event.internalEventListenerFactory
```
* 从输出可以看到容器中已经包含所需要的beanDefinition

#### 3-2 常见的BeanFactory后置处理器自定义实现
[所有测试代码](https://gitee.com/kfcuj/spring_learning/tree/master/source_code/src/main/java/com/village/dog/BeanFactoryPostProcessor)

**需求1：定义BeanFactory后置处理器用于解析@ComponentScan注解**
```
具体地，该后置处理器加载指定目录的所有class文件，
并将被（@Component,@Controller等）注解的class的beanDefinition添加到IOC容器中。
```
* 解析@Component注解的BeanFactoryPostProcessor的源码
```java
package com.village.dog.BeanFactoryPostProcessor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.stereotype.Component;

public class ComponentScanPostProcessor implements BeanFactoryPostProcessor {

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        try{
            ComponentScan componentScan = AnnotationUtils.findAnnotation(Config.class,ComponentScan.class);
            if(componentScan != null){
                for(String p:componentScan.basePackages()){     // 获取@ComponentScan的包名
                    // com.village.dog.BeanFactoryPostProcessor.component -> com/village/dog/BeanFactoryPostProcessor/component
                    // classpath*:com/village/dog/BeanFactoryPostProcessor/component/**/*.class
                    String path = "classpath*:" + p.replace(".","/") + "/**/*.class";
                    // 加载特定path下的所有class字节码资源文件
                    Resource[] resources = new PathMatchingResourcePatternResolver().getResources(path);
                    // 用于读取类字节码文件的类
                    CachingMetadataReaderFactory factory = new CachingMetadataReaderFactory();
                    // 生成BeanName的工具类
                    AnnotationBeanNameGenerator generator = new AnnotationBeanNameGenerator();

                    for(Resource r:resources){
                        // 通过类文件读取器获得注解的元数据
                        MetadataReader reader = factory.getMetadataReader(r);
                        AnnotationMetadata annotationMetadata = reader.getAnnotationMetadata();

//                        System.out.println("类名"+reader.getClassMetadata().getClassName());
//                        System.out.println("类上是否有@Component注解:" + reader.getAnnotationMetadata().hasAnnotation(Component.class.getName()));
//                        System.out.println("类上是否有@Component的派生注解:" + reader.getAnnotationMetadata().hasMetaAnnotation(Component.class.getName()));

                        if(annotationMetadata.hasAnnotation(Component.class.getName()) || annotationMetadata.hasMetaAnnotation(Component.class.getName())){
                            // 基于类的元数据构建beanDefinition
                            AbstractBeanDefinition bd = BeanDefinitionBuilder.genericBeanDefinition(reader.getClassMetadata().getClassName()).getBeanDefinition();
                            // DefaultListableBeanFactory是ConfigurableListableBeanFactory接口的实现类，因此需要向下转型
                            DefaultListableBeanFactory bf = (DefaultListableBeanFactory)beanFactory;
                            // 生成beanDefinition的名称
                            String beanName = generator.generateBeanName(bd,bf);
                            // 在容器中注册beanDefinition是ConfigurableListableBeanFactory
                            bf.registerBeanDefinition(beanName,bd);
                        }
                    }
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }
}

```
* 测试代码
```java
package com.village.dog.BeanFactoryPostProcessor;

import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.stereotype.Component;
import java.io.IOException;
/*
   需求：自定义BeanFactory后置处理器用于解析@ComponentScan注解
 */
public class Application1 {
    public static void main(String[] args) throws IOException {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean("config", Config.class);

        /*
            方式1:自己定义法解析@ComponentScan，实现注解的容器类的beanDefinition添加到容器中
        */
        // addBeanDefinition(context);

        /*
            方式2:spring中通过模板方法提供BeanFactoryPostProcessor的扩展接口，
            因此可以自定义BeanFactoryPostProcessor，从而自己实现@ComponentScan注解的解析
         */
        context.registerBean(ComponentScanPostProcessor.class);

        context.refresh();    // 启动容器
        for(String name:context.getBeanDefinitionNames()){ // 打印容器中的BeanDefinition
            System.out.println(name);
        }
        context.close();     // 销毁容器
    }

    // 不足：下面实现中ComponentScan注解的类是写死的，实际Spring框架应该主动的扫描包从而获得所有拥有注解的class
    public static void addBeanDefinition(GenericApplicationContext context){
        try{
            ComponentScan componentScan = AnnotationUtils.findAnnotation(Config.class,ComponentScan.class);
            if(componentScan != null){

                for(String p:componentScan.basePackages()){     // 获取@ComponentScan的包名
                    // com.village.dog.BeanFactoryPostProcessor.component -> com/village/dog/BeanFactoryPostProcessor/component
                    // classpath*:com/village/dog/BeanFactoryPostProcessor/component/**/*.class
                    String path = "classpath*:" + p.replace(".","/") + "/**/*.class";
                    // 通过容器类加载特定path下的所有class字节码资源文件
                    Resource[] resources = context.getResources(path);
                    // 用于读取类字节码文件的类
                    CachingMetadataReaderFactory factory = new CachingMetadataReaderFactory();
                    // 生成BeanName的工具类
                    AnnotationBeanNameGenerator generator = new AnnotationBeanNameGenerator();

                    for(Resource r:resources){
                        // 通过类文件读取器获得注解的元数据
                        MetadataReader reader = factory.getMetadataReader(r);
                        AnnotationMetadata annotationMetadata = reader.getAnnotationMetadata();

                        System.out.println("类名"+reader.getClassMetadata().getClassName());
                        System.out.println("类上是否有@Component注解:" + reader.getAnnotationMetadata().hasAnnotation(Component.class.getName()));
                        System.out.println("类上是否有@Component的派生注解:" + reader.getAnnotationMetadata().hasMetaAnnotation(Component.class.getName()));

                        if(annotationMetadata.hasAnnotation(Component.class.getName()) || annotationMetadata.hasMetaAnnotation(Component.class.getName())){
                            // 基于类的元数据构建beanDefinition
                            AbstractBeanDefinition bd = BeanDefinitionBuilder.genericBeanDefinition(reader.getClassMetadata().getClassName()).getBeanDefinition();
                            DefaultListableBeanFactory beanFactory = context.getDefaultListableBeanFactory();
                            // 生成beanDefinition的名称
                            String beanName = generator.generateBeanName(bd,beanFactory);
                            // 在容器中注册beanDefinition
                            beanFactory.registerBeanDefinition(beanName,bd);
                        }
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
```
* 执行日志
```java
[2022-07-05 22:15:15] [DEBUG] -- Refreshing org.springframework.context.support.GenericApplicationContext@36d4b5c
[2022-07-05 22:15:15] [DEBUG] -- Creating shared instance of singleton bean 'com.village.dog.BeanFactoryPostProcessor.ComponentScanPostProcessor'
[2022-07-05 22:15:15] [DEBUG] -- Creating shared instance of singleton bean 'config'
[2022-07-05 22:15:15] [DEBUG] -- Creating shared instance of singleton bean 'bean2'
[2022-07-05 22:15:15] [DEBUG] -- Bean2被容器管理!
[2022-07-05 22:15:15] [DEBUG] -- Creating shared instance of singleton bean 'bean3'
[2022-07-05 22:15:15] [DEBUG] -- Bean3被容器管理!
config
com.village.dog.BeanFactoryPostProcessor.ComponentScanPostProcessor
bean2
bean3
```
总结：可以发现自定义的用于解析@ComponentScan的工厂后处理器的基本思路是：

1)从注解中获得扫描class路径。 

2）加载class路径下的所有的class资源文件。 

3）通过@Component及派生注解筛选出class,为其构造beanDefinition放入到容器中。

执行日志的打印结果也表明自定义的后置处理器能够将被注解的Bean2和Bean3的beanDefinition生成并注册到容器中。

***
**需求2：定义BeanFactory的后置处理器用于解析@Configuration注解**
```
Spring中@Configuration注解的配置类充当工厂角色，配置类中被@Bean注解的方法充当工厂方法。
```


***
**需求3：定义BeanFactory的后置处理器用于解析@Mapper注解**



**MapperPostProcessor的代码**
```java
package com.village.dog.BeanFactoryPostProcessor;
import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.mapper.MapperFactoryBean;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;

/*/*
   BeanPostProcessor是高层接口，没有registerBeanDefinition方法
   而BeanDefinitionRegistryPostProcessor接口中有该方法。
   DefaultLisableBeanFactory是上述两个接口的实现类。
 **/
public class MapperPostProcessor implements BeanDefinitionRegistryPostProcessor {

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        BeanDefinitionRegistry beanFactory = registry;
        // 通过通配符资源解析器来加载所有的mapper接口的class文件
        try{
            PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
            Resource[] resources = resolver.getResources("classpath:com/village/" +
                    "dog/BeanFactoryPostProcessor/mapper/**/*.class");

            AnnotationBeanNameGenerator generator = new AnnotationBeanNameGenerator();
            CachingMetadataReaderFactory factory = new CachingMetadataReaderFactory();
            for(Resource resource:resources){
                MetadataReader reader = factory.getMetadataReader(resource);
                AnnotationMetadata annotationMetadata = reader.getAnnotationMetadata();
                ClassMetadata classMetadata = reader.getClassMetadata();
                if(classMetadata.isInterface() && annotationMetadata.hasAnnotation(Mapper.class.getName())){
                    AbstractBeanDefinition bd = BeanDefinitionBuilder.genericBeanDefinition(MapperFactoryBean.class)
                            .addConstructorArgValue(classMetadata.getClassName())
                            .setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_TYPE)   // 构造参数的自动装配
                            .getBeanDefinition();
                    /*
                          问题：Spring中如何对生成不同MapperFactoryBean<mapper>的beanName?
                              仅仅采用AnnotationBeanNameGenerator不能够对不同的mapper接口进行区分，原因在
                          于基于MapperFactoryBean<mapper>产生的beanName相同,由于容器采用key-value结构存储，beanName
                          相同会导致出现覆盖。

                          Spring的解决策略：对每个maper生成一个beanDefinition类，这个类不会注册到容器中，而是仅仅用于
                                   生成beanName,这样beanName就不会相同
                     */

                    // 此处生成的beanDefinition仅仅用于生成名称
                    AbstractBeanDefinition bd2 = BeanDefinitionBuilder
                            .genericBeanDefinition(classMetadata.getClassName())
                            .getBeanDefinition();
                    String beanName = generator.generateBeanName(bd2,beanFactory);
                    beanFactory.registerBeanDefinition(beanName,bd);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
    }
}
```

**总结：** Spring与Mybatis整合时，对于Mapper解析的基本思路：扫描classPath，将扫描到
的class文件进行筛选，从而找到@Mapper注解的interface class。将筛选出的每个mapper接口，封装到MapperFactoryBean<mapper>中，实际上
每个mapper都对应一个Bean工厂，spring容器最终存储的是不同mapper的Bean工厂的BeanDefinition。


P26
### 三 Aware和InitializingBean接口

### 四  @Autowird接口失效原因分析



### 参考资料

[Spring Boot注解Bean及其装配原理](https://www.cnblogs.com/jyd0124/p/springboot.html)






